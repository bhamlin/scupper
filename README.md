# Scupper
> a hole in a ship's side to carry water overboard from the deck.

## Purpose
To quickly and easily send small files, using ngrok, with rust. This is probably stupid.

## Design
Built as a library to allow for easy inclusion into other projects. An example of a 
standalone executable is provided as an example, and can be found once built in the
examples output directory.

You can build the example with `cargo build --example scupper` or `cargo build --examples`.

## Usage
- Get an account at [ngrok](https://ngrok.com/)
- Set your token to the environment variable `NGROK_AUTHTOKEN` or provide it on the 
  command line
- Run directly or with cargo, accepts one argument (a path to a file you want to serve)
  - `cargo run --examples scupper -- ~/Downloads/duckroll.gif`
  - or
  - `target/[debug|release]/examples/scupper ngrok-auth-token ~/Downloads/duckroll.gif`
- The file will be accessible to anyone who uses the url.
- RIP your ngrok bandwidth.
