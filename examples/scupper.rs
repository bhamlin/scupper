use clap::Parser;
use scupper::{error::ScupperError, EmptyResult, ScupperTunnel};

#[tokio::main]
async fn main() -> EmptyResult<ScupperError> {
    // Load provided environment values
    dotenv::dotenv().ok();

    // get options from path
    let opt = CLOpts::parse();

    // build our application with a single route
    let scupper_tunnel = ScupperTunnel::new_tunnel(&opt.path, &opt.auth_token).await?;

    println!(
        "Serving {} on URL: {}",
        scupper_tunnel.get_file_path(),
        scupper_tunnel.get_share_url()
    );

    scupper_tunnel.serve().await
}

#[derive(Parser)]
struct CLOpts {
    #[clap(env = "NGROK_AUTHTOKEN")]
    /// Ngrok auth token
    pub auth_token: String,

    #[clap(env = "SCUPPER_SHARE_PATH")]
    /// Path to file to serve
    pub path: std::path::PathBuf,
}
