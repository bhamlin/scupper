pub mod error;

use axum::{
    body::StreamBody,
    extract::ConnectInfo,
    extract::State,
    http::{header, HeaderMap, HeaderValue, StatusCode},
    response::IntoResponse,
    routing::get,
    Router,
};
use error::ScupperError;
use error_stack::{Result, ResultExt};
use ngrok::{prelude::*, tunnel::HttpTunnel, Session};
use std::{net::SocketAddr, path::PathBuf};
use tokio_util::io::ReaderStream;

pub type EmptyResult<T> = error_stack::Result<(), T>;

#[derive(Clone, Default)]
pub struct StaticServerConfig {
    pub path: PathBuf,
}

pub struct ScupperTunnel {
    pub router: Router,
    pub tunnel: HttpTunnel,

    file_path: String,
    share_url: String,
}

impl ScupperTunnel {
    pub async fn new_tunnel<P, S>(path: P, authtoken: S) -> Result<ScupperTunnel, ScupperError>
    where
        P: Into<PathBuf>,
        S: Into<String>,
    {
        let path: PathBuf = path.into();

        let router = build_router(&path);
        let session = build_session(authtoken).await?;
        let tunnel = session_to_tunnel(session).await?;

        let file_path = path.display().to_string();
        let share_url = tunnel.url().to_string();

        Ok(ScupperTunnel {
            router,
            tunnel,
            file_path,
            share_url,
        })
    }

    pub fn get_file_path(&self) -> &str {
        &self.file_path
    }

    pub fn get_share_url(&self) -> &str {
        &self.share_url
    }

    pub async fn serve(self) -> EmptyResult<ScupperError> {
        // Instead of binding a local port like so:
        // axum::Server::bind(&"0.0.0.0:8000".parse().unwrap())
        // Run it with an ngrok tunnel instead!
        axum::Server::builder(self.tunnel)
            .serve(
                self.router
                    .into_make_service_with_connect_info::<SocketAddr>(),
            )
            .await
            .change_context_lazy(|| ScupperError::AxumError)
    }
}

/// Build axum router for web handling
fn build_router<P: Into<PathBuf>>(path: P) -> Router {
    let path = path.into();

    Router::new()
        .route("/", get(serve))
        .fallback(serve)
        .with_state(StaticServerConfig { path })
}

/// Builds session using provided ngrok auth token
async fn build_session<S: Into<String>>(authtoken: S) -> Result<Session, ScupperError> {
    ngrok::Session::builder()
        .authtoken(authtoken)
        // Connect the ngrok session
        .connect()
        .await
        .change_context_lazy(|| ScupperError::NgrokConnection)
        .attach_printable_lazy(|| "Some issue connecting")
}

/// Consumes session to build tunnel
async fn session_to_tunnel(session: Session) -> Result<HttpTunnel, ScupperError> {
    session // Start a tunnel with an HTTP edge
        .http_endpoint()
        .listen()
        .await
        .change_context_lazy(|| ScupperError::NgrokConnection)
        .attach_printable_lazy(|| "Some issue with the tunnel")
}

async fn serve(
    State(cfg): State<StaticServerConfig>,
    ConnectInfo(remote_addr): ConnectInfo<SocketAddr>,
) -> impl IntoResponse {
    println!("Sending file to {remote_addr:?}");
    let path = cfg.path;
    let file = match tokio::fs::File::open(&path).await {
        Ok(file) => file,
        Err(err) => return Err((StatusCode::NOT_FOUND, format!("File not found: {}", err))),
    };

    let file_name = path.file_name().expect("File name conversion failed");

    // convert the `AsyncRead` into a `Stream`
    let stream = ReaderStream::new(file);
    // convert the `Stream` into an `axum::body::HttpBody`
    let body = StreamBody::new(stream);

    let mut headers = HeaderMap::new();
    headers.insert(
        header::CONTENT_TYPE,
        HeaderValue::from_static("application/binary"),
    );
    headers.insert(
        header::CONTENT_DISPOSITION,
        HeaderValue::from_str(format!("attachment; filename={:?}", file_name).as_str())
            .expect("Error converting filename to header value"),
    );

    Ok((headers, body))
}
