use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum ScupperError {
    AxumError,
    NgrokConnection,
}

impl Context for ScupperError {}
impl Display for ScupperError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            ScupperError::AxumError => "Issue serving",
            ScupperError::NgrokConnection => "Issue with ngrok connection",
        };

        write!(f, "{message}")
    }
}
